# Alpine Linux Docker Images
This repository contains my [Continuous Integration (CI)](https://docs.gitlab.com/ee/ci/) for building Alpine Linux Docker images with certain sets of tools. Between CI pipelines and server jobs that depend on a few programs, tailored Alpine images are a good alternative to a full Ubuntu or Debian installation.

The CI facilitates easily creating a new image with a set of specified packages, as well as arbitrary target architecture(s) thanks to [dind-buildx](https://gitlab.com/CodingKoopa/dind-buildx).
